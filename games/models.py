# -*- coding: utf-8 -*-
from django.db import models

class RodzajGry(models.Model):
    nazwa = models.CharField(max_length=30)
    class Meta:
        ordering = ['nazwa']
    def __unicode__(self):
        return self.nazwa

class Gra(models.Model):
    nazwa = models.CharField(max_length=30)
    opis = models.CharField(max_length=255, blank=True, null=True)
    rodzaj = models.ForeignKey('RodzajGry', related_name='gry')
    graczy_online = models.IntegerField(default=0)
    def __unicode__(self):
        return self.nazwa
