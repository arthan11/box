from django.shortcuts import render_to_response
from django.db.models import Sum
from games.models import *

def main_data():
    data = {}
    data['graczy_online'] = Gra.objects.all().aggregate(Sum('graczy_online'))['graczy_online__sum'] or 0
    return data

def index(request):
    data = main_data()
    data['title'] = 'Box - Gry online'
    data['rodzaje_gier'] = RodzajGry.objects.all()
    return render_to_response('index.html', data)

def game(request, game_id):
    data = main_data()
    gra = Gra.objects.get(pk=game_id)
    data['gra'] = gra
    data['title'] = u'{} ONLINE, zagraj - Box'.format(gra.nazwa)

    return render_to_response('gra.html', data)

def tables(request, game_id):
    data = main_data()
    gra = Gra.objects.get(pk=game_id)
    data['gra'] = gra
    data['title'] = u'{}'.format(gra.nazwa)

    return render_to_response('tables.html', data)