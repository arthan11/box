from django.contrib import admin
import inspect
import games.models


for name, obj in inspect.getmembers(games.models):
    if inspect.isclass(obj):
        admin.site.register(obj)